import React, { useContext } from 'react'
import LearnUseContext1 from './LearnUseContext1'
import { InfoContext } from '../../App'

const LearnUseContext = () => {
  let contextData=useContext(InfoContext)
 
  return (
    <div>
      Learning UseContext
      <br></br>
      {contextData}
      <br></br>
      <LearnUseContext1></LearnUseContext1>
    </div>
  )
}

export default LearnUseContext
