import React, { useContext } from 'react'
import { CountContext, InfoContext } from '../../App'

const LearnUseContext1 = () => {
  let contextData = useContext(InfoContext)
  let countContextData  = useContext(CountContext) 
  return (
    <div>
      {contextData}
      <br></br>
      <div>
        data From CountContext <br></br>
        {countContextData.count} <br></br>
        
      </div>
      <div>
        <button onClick={()=>{
          countContextData.setCount(countContextData.count+1)
        }}>Count</button>
      </div>

    </div>
  )
}

export default LearnUseContext1
