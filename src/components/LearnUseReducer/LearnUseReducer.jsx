import React, { useReducer } from 'react'

const LearnUseReducer = () => {
    let initValue=0
    let reduce=()=>{
        return 9
    }
    let [state,dispatch]=useReducer(reduce,initValue)
  return (
    <div>
        {state}
        <button onClick={()=>{
            dispatch()
        }}>click me</button>
     
    </div>
  )
}

export default LearnUseReducer
