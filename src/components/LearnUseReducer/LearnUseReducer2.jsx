import React, { useReducer } from 'react'

const LearnUseReducer2 = () => {
    let initValue=0
    let reducer = (state,action) => {
        if (action.type==="increment"){return state+1}
        else if(action.type==="decrement"){return state-1}
        else if(action.type==="reset"){return state=initValue}
       
        
        return state
       

    }
    let [count,dispatch]=useReducer(reducer,initValue)
 
  return (
    <div>

 Learn useReducer
          <br></br>
          <br></br>
 <div className='reducer'>
    <div>
        {count}
    </div>
  
    <button onClick={()=>{
        dispatch({type:"increment"})
    }}>Increment</button><br></br>
    
    <button onClick={()=>{
        dispatch({type:"decrement"})
    }}>decrement</button><br></br>
    <button onClick={()=>{
        dispatch({type:"reset"})
    }}>Reset</button>
 </div>
    </div>
  )
}

export default LearnUseReducer2
