import React, { useReducer } from 'react'

const PracticeUseReducer = () => {
let initialValue=0
let reducer=(state,action)=>{
    return 9
}
    let [count,dispatch]=useReducer(reducer,initialValue)

  return (
    <div>
        {count}
        Practice UseReducer
        <button onClick={()=>{
            dispatch()
        }}>Click</button>
      
    </div>
  )
}

export default PracticeUseReducer
