import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Contact from './Contact'



const Routing = () => {
  return (
    <div>
      <div className='navbar'>
        <NavLink className="link" to="/">Home</NavLink>
        <NavLink className="link" to="about">About</NavLink>
        <NavLink className="link" to="contact">Contact</NavLink>
      </div>

<div>
        <Routes>
          <Route path="/" element={<div><Home></Home></div>}></Route>
          <Route path="about" element={<div><About></About></div>}></Route>
          <Route path="contact" element={<div><Contact></Contact></div>}></Route>
        </Routes>
</div>

  
   </div>

  )
}

export default Routing
