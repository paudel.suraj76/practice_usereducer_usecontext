import React, { createContext, useState } from 'react'
import C21 from './C21'
import C22 from './C22'
import C23 from './C23'
export let PersonDetails = createContext()
const C2 = () => {

  let [info,setInfo] = useState({ name: "nitan", age: 29, address: "gagalphedi" })
 



  return (
    <div>
      {/* to share the context we need to wrap with above created PersonalDetails */}
      <PersonDetails.Provider value={{info:info,setInfo:setInfo}}>
        <C21></C21>
        <C22></C22>
        <C23></C23>
      </PersonDetails.Provider>

    </div>
  )
}

export default C2
